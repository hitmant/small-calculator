# Scalc Makefile
SHELL = /bin/bash
CXX = g++
CXXFLAGS = -g -Wall -std=gnu++2a
OBJDIR = obj
SRCDIR = src
vpath %.cc $(SRCDIR)
vpath %.hh $(SRCDIR)

SRCS_CC = main.cc compute.cc functions.cc
SRCS = $(SRCS_CC)
OBJS = $(SRCS_CC:.cc=.o)


scalc : $(addprefix $(OBJDIR)/,$(OBJS)) $(SRCS)
	$(CXX) $(CXXFLAGS) -o $@ $(filter %.o, $^)



# Header file dependencies
$(OBJDIR)/main.o : compute.hh functions.hh
$(OBJDIR)/compute.o : compute.hh functions.hh constants.hh


$(OBJDIR)/%.o : %.cc
	if ! [[ -d $(OBJDIR) ]]; then mkdir $(OBJDIR); fi
	$(CXX) $(CXXFLAGS) -c -o $@ $<

.PHONY: clean
clean :
	if [[ -d $(OBJDIR) ]]; then rm -dr $(OBJDIR); fi
	if [[ -a scalc ]]; then rm scalc; fi
