# scalc - A Small Calculator Project
[![Version](https://img.shields.io/badge/Version-0.4.0-red.svg)](https://shields.io/)
[![Licence](https://img.shields.io/badge/Licence-GPLv3.0-blue.svg)](https://shields.io/)

## About
This is a small calculator project, a refreshing hello world for me in C++ to familiarize
myself with the new standards. This is intended to eventually be a useful CLI calculator
with standard features such as variables, history and functions. Far-future features may
include a scripting language for making custom functions and support for non-numerical
calculations, vectors and matrices.

## Current features:
- Basic calculations, e.g. 2 + 5\*6 - 3^2
- Somewhat smart interpreting, i.e. `1+2*3` is 1 + ( 2 \* 3 )
- Scientific notation with the 'E' character: `-6.45E-23` is -6.45\*10^(-23)
- Some constants: e, pi and phi.
- Implied multiplication with constants: `2e` is 2 \* e
- Implied addition with negative numbers: `5 -3` is 5 - 3
- Parenthesis support.
- Variables with the = operator: `x = 12^3` means "assign 12^3 to x to be used later."
- Reference to previous answer via "ans"-variable;


## Planned features:
- History.
- Built-in functions.
- User defined functions.
- Scripting language for defining functions and constants.
- Vector variables and calculations.
- Matrix variables and calculations.
- Complex variables and calculations.
- Quaternion variables and calculations.

## Building
This program is developed with GCC 10.1.0, GNU Make 4.3 and uses features from GCC
experimental implementation of the C++20 standard (`-std=gnu++2a`).
To build simply run `make` and move the resulting executable
`scalc` to your `$PATH`, if for some reason one wants to have this program installed.
