#include <string>
#include "functions.hh"

bool isoperator(const char& c) {
	/** Assignment operator not defined in precedence map as it
	 *  is not a legal part of an expression, and thus has no
	 ** effective precedende. */
	return precedence.contains(std::string(1, c)) || c == '=';
}
