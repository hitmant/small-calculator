#ifndef CONSTANTS_HH
#define CONSTANTS_HH
#include <numbers>

const std::map<std::string, long double> constants = {
	{"e", std::numbers::e},
	{"pi", std::numbers::pi},
	{"phi", std::numbers::phi}
};

#endif
