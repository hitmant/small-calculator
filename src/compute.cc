#include <limits>
#include <string>
#include <cmath>
#include <map>
#include <vector>
#include <iostream>
#include "compute.hh"
#include "functions.hh"
#include "constants.hh"

static std::map<std::string, long double> variables;

long double compute(const svecit inp_beg, const svecit inp_end) {

	/** Part 0: Handle input of size less than 3.
	 *   Cases 0 and 2 should not be encountered normally; testing needed
	 *   to determine necessity.
	 *   Case 1 prints error message if input is not a defined variable,
	 **  constant or a valid numeric literal. Invalidates result as NAN. */
	if (inp_end - inp_beg == 0) return 0.0D;
	if (inp_end - inp_beg == 1) {
		if (constants.contains(*inp_beg)) return constants.at(*inp_beg);
		if (variables.contains(*inp_beg)) return variables.at(*inp_beg);
		try {
			return std::stod(*inp_beg);
		} catch (...) {
			std::cerr << "Invalid argument: " << *inp_beg << "; " << std::flush;
			return NAN;
		}
	}
	if (inp_end - inp_beg == 2) { //case "x", "-y"
		try {
			return std::stod(*inp_beg) + std::stod(*inp_beg);
		} catch (...) {
			std::cerr << "Invalid argument! (perhaps a formatting error); " << std::flush;
			return NAN;
		}
	}

	/** Part 1: Compute parenthesised subexpressions.
	 *   Can't edit input vector, so save non-parenthesised tokens and results
	 *   of parenthesised expressions into noparens vector to be computed in
	 **  subsequent parts. */
	std::vector<std::string> noparens;
	unsigned par_level = 0;
	for (auto beg=inp_beg, end=inp_beg; end != inp_end; ++end) {
		if (*end == "(") {++par_level; if (par_level==1) beg = end+1;}
		else if (*end == ")") {
			if (par_level == 1) {
				noparens.push_back(
				    std::to_string(compute(beg, end)));
				--par_level;
			} else --par_level;
		} else if (!par_level) noparens.push_back(*end);
	}

	/** Part 2: Find lowest precedence operator
	 *   Start from the end and iterate right to left to keep left-right
	 **  associativity. */
	decltype(noparens.end()) low_op_it;
	std::size_t low_op_prec = std::numeric_limits<std::size_t>::max();
	for (auto it = noparens.end()-1; it != noparens.begin(); --it) {
		if (precedence.contains(*it) 
		    && low_op_prec > precedence.at(*it)) {
			low_op_it = it;
			low_op_prec = precedence.at(*it);
		}
	}

	/** Part 3: Recurse left
	 **  */
	long double lhs = compute(noparens.begin(), low_op_it);

	/** Part 4: Recurse right
	 **  Don't include the operator found in part 2. */
	long double rhs = compute(low_op_it+1, noparens.end());

	/** Part 5: Compute result
	 **  */
	return functions.at(*low_op_it)(lhs, rhs);
}

long double compute(std::vector<std::string>& input) {
	variables["ans"] = compute(input.begin(), input.end());
	return variables["ans"];
}

long double assign(const std::string& var, const svecit beg, const svecit end) {
	variables[var] = compute(beg, end);
	return variables[var];
}
