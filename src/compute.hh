#ifndef COMPUTE_HH
#define COMPUTE_HH

/** Typedef for shorter parameter declarations. */
typedef std::vector<std::string>::iterator svecit;

/** Main compute module function. Takes a string vector as input
 *  and computes the calculation it represents. Does not validate
 ** input, as that is the job of validator module. */
long double compute(std::vector<std::string>&);

/** Auxiliary compute module function. Used for assignment operations.
 *  Parameters are a string representation of the variable to be assigned
 *  and string vector to be passed to compute(). Returns the result
 ** of assignment. */
long double assign(const std::string&, const svecit, const svecit);

#endif
