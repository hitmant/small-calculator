#ifndef FUNCTIONS_HH
#define FUNCTIONS_HH

#include <map>
#include <functional>
#include <cmath>

/** Maps of all functions are defined in this header.
 *  Currently only binary functions are defined, once compute module
 *  is improved more function maps can be added. Rename functions to
 *  binary_functions or something. */
const std::map<std::string, std::function<long double(long double,long double)>> 
functions = {
	{"+", [](long double a, long double b) -> long double {return a+b;} },
	{"-", [](long double a, long double b) -> long double {return a-b;} },
	{"*", [](long double a, long double b) -> long double {return a*b;} },
	{"/", [](long double a, long double b) -> long double {return a/b;} },
	{"^", [](long double a, long double b) -> long double {return std::pow(a, b);} },
};

/** Map of operator precedences. Functions are evaluated as tokens
 *  with parenthesis precedence. All operators are left-to-right
 *  associative. */
const std::map<std::string, unsigned int> precedence = {
	{"+", 1}, {"-", 1},
	{"*", 2}, {"/", 2}, {"%", 2},
	{"^", 3}, 
	{"(", 4}, {")", 4},
};

bool isoperator(const char&);

#endif
