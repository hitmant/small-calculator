#include <iostream>
#include <iomanip>
#include <cctype>
#include <vector>
#include <algorithm>
#include "compute.hh"
#include "functions.hh"

int main(int argc, char** argv) {
	const char* prompt = "> ";
	std::cout << "SmallCalc started, enter your input:" << std::endl;
	std::cout << prompt << std::flush;

	std::string in_line;
	while (getline(std::cin, in_line)) {

		if (in_line[0] == 'q') break; //quit program

		/** Parser module. Takes in_line and parses it into tokens vector
		 *  for compute module. Takes into account e-notation as well as
		 ** tokens with no space in between them. */
		std::vector<std::string> tokens;
		std::string curr_token("");

		for (const char& c : in_line) {
			if (isspace(c)){
				if (curr_token.size() > 0) tokens.push_back(curr_token);
				curr_token.clear();
			} else if (c == '+' || c == '-') {
				if (curr_token.back() == 'E' && curr_token.size() > 1) curr_token += c;
				else {
					if (curr_token.size() > 0) tokens.push_back(curr_token); 
					if (c == '+') {
						tokens.push_back(std::string(1, c));
						curr_token.clear();
					} else {
						curr_token = std::string(1, c);
					}
				}
			} else if (isoperator(c)) {
				if (c == '.' || c == ',') {curr_token += c; continue;}
				if (curr_token.size() > 0) tokens.push_back(curr_token);
				tokens.push_back(std::string(1, c)); 
				curr_token.clear();
			} else if (isalpha(c) && curr_token.size() > 0 && 
			           isdigit(curr_token.back()) && c != 'E') {
				tokens.push_back(curr_token);
				tokens.push_back("*");
				curr_token = std::string(1, c);
			} else {
				curr_token += c;
			}
		}
		if (!curr_token.empty()) tokens.push_back(curr_token);
		/* End of parser module. */

		/** Validator module. Currently finds negated tokens and parenthesis not
		 *  preceded or succeded by an operator token example user input: "2e -4" 
		 *  reformatted to "2e + -4" by this validator to keep the structure of 
		 ** the calculation. */
		std::size_t pos = 0;
		for (auto it = tokens.begin(); it != tokens.end(); ++it, ++pos) {
			if ((*it)[0] == '-' && it->size() > 1 && !isoperator((it-1)->back())) {
				tokens.emplace(it, "+");
				it = tokens.begin() + (++pos);
			} else if (*it == "(" && it != tokens.begin() && !isoperator((it-1)->back())) {
				tokens.emplace(it, "*");
				it = tokens.begin() + (++pos);
			} else if (*it == ")" && it != tokens.end()-1 && !isoperator((it+1)->back())) {
				tokens.emplace(it+1, "*");
				it = tokens.begin() + pos + 2;
				pos += 2;
			}
		} /* End of validator module. */

		/** Print appropriate output, i.e. acknowledgement of assignment or just the
		 *  result. Functions append() and compute() are part of the compute module
		 *  as defined in compute.hh. */
		std::cout << std::uppercase << "\t" 
		          << std::setprecision(std::numeric_limits<long double>::digits10 + 1);
		if (tokens.size() > 1 && tokens[1] == "=") {
			std::cout << tokens[0] << " = " 
			          << assign(tokens[0], tokens.begin()+2, tokens.end());
		} else {
			std::cout << std::uppercase << compute(tokens);
		}
		std::cout << std::setprecision(6) << std::nouppercase << std::endl 
		          << prompt << std::flush;

		/* End of calculation for this input, loop to get next input. */
	}
}
